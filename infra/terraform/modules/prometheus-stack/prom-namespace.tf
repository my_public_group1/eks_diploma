resource "kubernetes_namespace" "prometheus" {
  metadata {
    name = "${var.prom_namespace}"
  }
}
