resource "kubernetes_service" "grafana" {
  depends_on = [kubernetes_namespace.prometheus]
  metadata {
    name = "grafana-external"
    namespace = var.prom_namespace
    annotations = {
      "service.beta.kubernetes.io/aws-load-balancer-backend-protocol" = "http"
      "service.beta.kubernetes.io/aws-load-balancer-ssl-cert" = "${var.acm_arn}"
      "service.beta.kubernetes.io/aws-load-balancer-ssl-ports" = "https"
    }
  }
  spec {
    selector = {
      "app.kubernetes.io/name" = "grafana"
    }
    port {
      name        = "http"
      port        = var.ports.http_tcp[0]
      target_port = var.ports.grafana_tcp[0]
    }
    port {
      name        = "https"
      port        = var.ports.https_tcp[0]
      target_port = var.ports.grafana_tcp[0]
    }
    type = "LoadBalancer"
  }
}
