variable "prom_namespace" {
  type = string
  default = "monitoring"
}

variable "grafana_password" {
  type = string
}

variable "acm_arn" {
  type = string
}

variable "ports" {
  type = map(list(string))
}
