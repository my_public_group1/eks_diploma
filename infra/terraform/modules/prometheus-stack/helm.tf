resource "helm_release" "prometheus" {
  depends_on = [kubernetes_namespace.prometheus, helm_release.loki]
  name             = "prometheus-${terraform.workspace}"
  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "kube-prometheus-stack"
  namespace        = "${var.prom_namespace}"

  values = [templatefile("${path.module}/values.yaml", {  
             workspace = "${terraform.workspace}" 
           })]

  set {
    name  = "grafana.adminPassword"
    value = "${var.grafana_password}"
  }

  set {
    name = "alertmanager.persistentVolume.storageClass"
    value = "gp2"
  }

  set {
    name = "server.persistentVolume.storageClass"
    value = "gp2"
  }

}

resource "helm_release" "loki" {
  depends_on = [kubernetes_namespace.prometheus]
  name             = "loki-${terraform.workspace}"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "loki-stack"
  namespace        = "${var.prom_namespace}"

  set {
    name  = "grafana.sidecar.datasources.enabled"
    value = "false"
  }
}


