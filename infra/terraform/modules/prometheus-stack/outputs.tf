output "grafana_dns_name" {
  description = "Endpoint for Grafana"
  value       = kubernetes_service.grafana.status.0.load_balancer.0.ingress.0.hostname
}
