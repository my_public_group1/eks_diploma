resource "kubernetes_config_map" "grafana_dashboard" {
  metadata {
    name = "grafana-dashboard"
    namespace = var.prom_namespace
    labels = {
      grafana_dashboard = "1"
    }
  }
  data = {
    "loki-dashboard.json" = file("${path.module}/loki-dashboard.json")
  }
}
