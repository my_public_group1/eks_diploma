resource "kubernetes_service" "frontend" {
  depends_on = [kubernetes_namespace.onlineboutique]
  metadata {
    name = "frontend-external"
    namespace = var.app_namespace
    annotations = {
      "service.beta.kubernetes.io/aws-load-balancer-backend-protocol" = "http"
      "service.beta.kubernetes.io/aws-load-balancer-ssl-cert" = "${var.acm_arn}"
      "service.beta.kubernetes.io/aws-load-balancer-ssl-ports" = "https"
    }
  }
  spec {
    selector = {
      "app" = "frontend"
    }
    port {
      name        = "http"
      port        = var.ports.http_tcp[0]
      target_port = var.ports.frontend_tcp[0]
    }
    port {
      name        = "https"
      port        = var.ports.https_tcp[0]
      target_port = var.ports.frontend_tcp[0]
    }

    type = "LoadBalancer"
  }
}
