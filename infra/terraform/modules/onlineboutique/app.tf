resource "helm_release" "microservices" {
  depends_on = [kubernetes_namespace.onlineboutique]
  name             = "microservices-${terraform.workspace}"
  repository       = "oci://us-docker.pkg.dev/online-boutique-ci/charts/"
  chart            = "onlineboutique"
  namespace        = "${var.app_namespace}"

  set {
    name  = "frontend.externalService"
    value = "false"
  }

}

