resource "kubernetes_horizontal_pod_autoscaler_v2" "frontend" {
  metadata {
    name = "frontend"
    namespace = var.app_namespace
  }

  spec {
    min_replicas = 1
    max_replicas = 5
    scale_target_ref {
      kind = "Deployment"
      name = "frontend"
      api_version = "apps/v1"
    }
    metric {
      type = "Resource"
      resource {
        name = "cpu"
        target {
          type = "Utilization"
          average_utilization = 50
	}
      }
    }
  }
}
