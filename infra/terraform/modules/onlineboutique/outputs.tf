output "app_dns_name" {
  description = "Endpoint for APP"
  value       = kubernetes_service.frontend.status.0.load_balancer.0.ingress.0.hostname
}
