variable "app_namespace" {
  type = string
  default = "onlineboutique"
}

variable "acm_arn" {
  type = string
}

variable "ports" {
  type = map(list(string))
}
