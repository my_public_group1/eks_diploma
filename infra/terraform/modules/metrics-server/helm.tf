resource "helm_release" "metrics_server" {
  name             = "metrics-server-${terraform.workspace}"
  repository       = "https://kubernetes-sigs.github.io/metrics-server/"
  chart            = "metrics-server"
  namespace        = "kube-system"
}
