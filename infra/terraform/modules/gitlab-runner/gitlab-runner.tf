resource "helm_release" "gitlab-runner" {
  depends_on       = [kubernetes_namespace.gitlab-runner]
  name             = "gitlab-runner-${terraform.workspace}"
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-runner"
  version          = var.chart_version
  namespace        = var.gitlab_runner_namespace

  values = [
    templatefile("${path.module}/tmpl/values.yaml", {
      gitlab_runner_token = var.gitlab_runner_token
      gitlab_runner_concurrent_agents = var.gitlab_runner_concurrent_agents
    })
  ]
}
