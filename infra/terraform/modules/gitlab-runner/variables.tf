variable "CI_RUNNER_REVISION" {
  description = "Gitlab Runner Revision for helper image"
  default     = "x86_86ad88ea"
}

variable "gitlab_runner_concurrent_agents" {
  description = "Gitlab Runner Concurrent Agent Configurations"
  default     = 10
}

variable "gitlab_runner_token" {
  description = "Gitlab Runner Registration Token"
}

variable "gitlab_runner_namespace" {
  type = string
  default = "gitlab-runner"
}

variable "chart_version" {
  type = string
  default = "0.56.0"
}
