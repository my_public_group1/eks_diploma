resource "kubernetes_namespace" "gitlab-runner" {
  metadata {
    name = var.gitlab_runner_namespace
  }
}

