resource "kubernetes_cluster_role_binding" "gitlab-runner" {
  metadata {
    name = "gitlab-runner"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "default"
    namespace = "gitlab-runner"
  }
}

