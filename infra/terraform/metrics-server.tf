module "metrics_server" {
  source     = "./modules/metrics-server"
  depends_on = [null_resource.wait_eks_cluster]
}
