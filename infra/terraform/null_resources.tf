resource "null_resource" "wait_eks_cluster" {
  depends_on = [module.eks]

  provisioner "local-exec" {
    command = "sleep 1m"
  }
}

resource "null_resource" "remove_ecr_images" {
  depends_on = [module.ecr]

  triggers = {
    region   = var.aws_region
    ecr_repo = var.ecr_repo_name
  }

  provisioner "local-exec" {
    when    = destroy
    environment = merge(
      { "REGION"   = self.triggers.region },
      { "ECR_REPO" = self.triggers.ecr_repo })
    command = "${path.root}/scripts/remove-images-from-ecr.sh"
  }
}

