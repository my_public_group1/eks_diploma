module "gitlab_runner" {
  source     = "./modules/gitlab-runner"
  depends_on = [null_resource.wait_eks_cluster]

  gitlab_runner_token              = local.inputs.gitlab_runner_token
}
