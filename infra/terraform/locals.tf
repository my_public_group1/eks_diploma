locals {
  cluster_name = "test-eks-${random_string.suffix.result}"
  ports = {
    http_tcp         = ["80"],
    https_tcp        = ["443"],
    frontend_tcp     = ["8080"],
    grafana_tcp      = ["3000"]
  }
}
