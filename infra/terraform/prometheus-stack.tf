module "prometheus_stack" {
  source     = "./modules/prometheus-stack"
  depends_on = [null_resource.wait_eks_cluster, aws_eks_addon.ebs-csi]
  grafana_password = local.inputs.grafana_password
  acm_arn = module.acm.acm_certificate_arn
  ports = local.ports
}
