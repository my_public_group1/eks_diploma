module "acm" {
  source                    = "./modules/acm"
  domain_name               = local.inputs.public_dns_name
  subject_alternative_names = ["*.${local.inputs.public_dns_name}"]
  zone_id                   = data.aws_route53_zone.zone.zone_id
}
