#!/usr/bin/env bash

aws ecr batch-delete-image --region $REGION \
  --repository-name $ECR_REPO \
  --image-ids "$(aws ecr list-images --region $REGION --repository-name $ECR_REPO --query 'imageIds[*]' --output json)" || true
