variable "bucket_name" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "ecr_repo_name" {
  type = string
  default = "private-ecr-eks-repo"
}
