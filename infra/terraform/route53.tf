resource "aws_route53_record" "frontend" {
  depends_on = [module.onlineboutique]
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "app"
  type    = "CNAME"
  ttl     = 5
  records = [module.onlineboutique.app_dns_name]
}

resource "aws_route53_record" "grafana" {
  depends_on = [module.prometheus_stack]
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "grafana"
  type    = "CNAME"
  ttl     = 5
  records = [module.prometheus_stack.grafana_dns_name]
}
