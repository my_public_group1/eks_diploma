module "onlineboutique" {
  source     = "./modules/onlineboutique"
  depends_on = [module.metrics_server]
  acm_arn = module.acm.acm_certificate_arn
  ports = local.ports
}
