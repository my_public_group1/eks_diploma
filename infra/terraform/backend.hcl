# backend.hcl
bucket                  = "skill-s3-backup-bucket"
region                  = "us-west-2"
dynamodb_table          = "skill-s3-backup-bucket-dynamodb"
key                     = "adv_dip/terraform.tfstate"      
encrypt                 = true
