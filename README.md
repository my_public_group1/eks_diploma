# EKS-Diploma

[![Build Status](https://gitlab.com/my_public_group1/eks_diploma/badges/main/pipeline.svg)](https://gitlab.com/my_public_group1/eks_diploma)

Данный проект является дипломной работой курса "DevOps-инженер. Kubernetes" и содержит в себе следующие этапы:

- Создание Kubernetes кластера [AWS EKS](https://aws.amazon.com/eks/) в облаке `Amazon AWS` с помощью инструмента `Terraform`;
- Конфигурация кластера: установка [AWS LoadBalancer Controller](https://github.com/aws/eks-charts/tree/master/stable/aws-load-balancer-controller), [Metrics-Server](https://github.com/kubernetes-sigs/metrics-server), [Google Onlineboutique](https://github.com/GoogleCloudPlatform/microservices-demo), [Prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack), [Loki-stack](https://github.com/grafana/loki/tree/main/production/helm/loki-stack), [Gitlab-Runner](https://gitlab.com/gitlab-org/charts/gitlab-runner);
- Сборка, загрузка образа приложения `LoadGenerator` в реестр [AWS ECR](https://aws.amazon.com/ecr/), а также удаление старых образов с помощью `Gitlab CI/CD`.

## Требования для запуска проекта:

1. Зарегистрируйте аккаунт [Amazon AWS](https://aws.amazon.com/account/sign-up);
2. Установите [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) для управления аккаунтом и ресурсами;
3. Перейдите в `Identity and Access Management (IAM)` создайте пользователя `Administrator`, создайте группу `Administrators` с политикой доступа `AdministratorAccess` и добавьте только что созданного пользователя в эту группу;
4. Далее перейдите в вашего пользователя `Administrator`, выберите вкладку `Security Credentials`, создайте `Access Key`, и сохраните у себя `AWS Access Key ID` и `AWS Secret Access Key`;
5. [Cконфигурируйте AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html), используя раннее полученные значения `AWS Access Key ID` и `AWS Secret Access Key`;
6. Купите домен у любого регистратора или зарегистрировать бесплатный, [создать hosted zone в AWS Route 53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html) и [прописать NS-сервера у регистратора домена](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-name-servers-glue-records.html);
7. Зарегистрируйте аккаунт [PagerDuty](https://www.pagerduty.com/sign-up/), создайте сервис получения уведомлений с интеграцией Prometheus, и сохраните у себя url и приватный ключ;
8. Установите [Terraform](https://www.terraform.io/downloads);

## Описание инфраструктуры проекта:

Инфраструктура проекта предусматривает создание следующих элементов:

| Элемент | Назначение |
| --- | ----------- |
| [AWS EKS](https://aws.amazon.com/eks/) | Kubernetes кластер. Состоит за компонентов `Control Plane` и рабочих нод. Имеет две автомастштабирующихся группы нод: первая группа от 1 до 3 нод, а вторая группа от 1 до 2 нод |
| [AWS Virtual Private Cloud](https://aws.amazon.com/vpc/) | Необходимо для создания изолированной сетевой инфраструктуры |
| [AWS Subnets](https://docs.aws.amazon.com/vpc/latest/userguide/configure-subnets.html) | Необходимы для разделения сетевых ресурсов на публичные и приватные. В данном случае создаются три группы подсетей в разных зонах доступности(приватная, публичная, приватная для работы компонентов `k8s Control Plane`) |
| [AWS ACM Certificates](https://aws.amazon.com/certificate-manager/) | Сертификаты необходимы для `AWS Application LoadBalancer Controller`, чтобы все публичные доступы по домену работали через протокол шифрования `HTTPS` |
| [AWS Internet Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html) | Необходим для связывания ресурсов публичных подсетей с Интернетом и обратно |
| [AWS NAT Gateway](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html) | Необходим для связывания ресурсов приватных подсетей с Интернетом. Он отправляет запросы от приватных подсетей как будто это ресурс из публичной подсети. |
| [AWS IAM](https://aws.amazon.com/iam/) | Используется для создания ролей доступа к различным ресурсам |
| [AWS Route53](https://aws.amazon.com/route53/) Records | DNS записи для приложения и [Grafana](https://grafana.com/) в системе Route53 |
| [AWS ECR](https://aws.amazon.com/ecr/) | Необходим для хранения образов `Kubernetes` |
> Таблица 1.

Также в кластере с помощью `Helm charts` устанавливаются и конфигурируются следующие компоненты:

| Компонент | Назначение |
| --- | ----------- |
| [AWS LoadBalancer Controller](https://github.com/aws/eks-charts/tree/master/stable/aws-load-balancer-controller) | Необходим для создания эндпоинтов на сервисах `Kubernetes` типа `LoadBalancer` |
| [Metrics-Server](https://github.com/kubernetes-sigs/metrics-server) | Сервер, собирающий метрики кластера. Необходим для работы `HorizontalPodAutoscaler`, который в зависимости от значения метрики увеличивает или уменьшает количество подов определенного деплоймента |
| [Google Onlineboutique](https://github.com/GoogleCloudPlatform/microservices-demo) | Приложение проекта. Представляет из себя набор микросервисов в `Kubernetes`, из которых работает система онлайн-магазина. Позволяет потренироваться с нагрузкой на сервисы кластера |
| [Prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack) | Стэк приложений для мониторинга кластера(Prometheus, AlertManager, Grafana, Node-exporter) |
| [Loki-stack](https://github.com/grafana/loki/tree/main/production/helm/loki-stack) | Стэк приложения для сбора логов в кластере(Loki, Promtail) |
| [Gitlab-Runner](https://gitlab.com/gitlab-org/charts/gitlab-runner) | Служит для работы с `Gitlab CI/CD`. Разворачивается как под кластера, поэтому не требует данных для работы с `Amazon AWS` |
> Таблица 2.

Общая схема взаимодействия между элементами инфраструктуры представлена ниже:

![scheme_process](pictures/EKS.png)

## Как работать с gitlab:

Необходимо сохранить у себя токен для создания gitlab-runner, для этого выполните следующие действия:

1. Перейдите `Settings -> CI/CD -> Runners`;

2. В области `Shared Runners` отключите флажок `Enable shared runners for this project`;

3. В области `Project Runners` найдите и нажмите кнопку `New project runner`.

4. Далее откроется окно конфигурации раннера. Оставьте флажок на `linux` и добавьте тэг `kubernetes`:
![scheme_process](pictures/gitlab-runner.png)

5. В следующем окне появится токен для регистрации раннера. Сохраните его у себя.
![scheme_process](pictures/gitlab-token.png)

## Как работать с проектом:
1. Склонируйте репозиторий к себе для работы с ним:

   ```shell
   git clone https://gitlab.com/my_public_group1/vpc_advanced_diploma.git
   ```
2. Далее необходимо сконфигурировать и запустить Terraform для создания корзины AWS S3 Bucket. Перейдите в директорию `infra/s3-terraform` и переименуйте файл `inputs.tf.example` в `inputs.tf`, после отредактриуйте следующие переменные:

| Переменная | Значение |
| --- | ----------- |
| bucket_name | Название корзины `AWS S3 Bucket` (должно быть уникально среди пользователей AWS, иначе будет ошибка) |
| dynamodb_name | Название базы данных `DynamoDB` (должно быть уникально среди пользователей AWS, иначе будет ошибка) |
> Таблица 3.

3. Инициализируйте Terraform и запустите создание ресурсов:

   ```shell
   terraform init
   terraform apply
   ```
4. Далее необходимо сконфигурировать и запустить Terraform для создания инфраструктуры проекта. Перейдите в директорию `infra/terraform` и переименуйте файл `inputs.tf.example` в `inputs.tf`, после отредактриуйте следующие переменные:

| Переменная | Значение |
| --- | ----------- |
| public_dns_name | Ваш домен, зона которого была сконфигрурирована в `Route 53` |
| gitlab_runner_token | Токен для регистрации `gitlab-раннера` |
| grafana_password | Пароль для входа в `Grafana` (user: admin) |

5. Инициализируйте Terraform с файлом конфигурации backend:

   ```shell
   terraform init -backend-config=backend.hcl
   ```

6. Запустите создание ресурсов AWS:

   ```shell
   terraform apply -var-file="s3.tfvars"
   ```
7. После создания ресурсов через `Terraform` вы получите вывод различных переменных. Используя эти переменные необходимо создать переменные проекта `Gitlab CI/CD`. Для этого выполните следующие шаги:
- Перейдите `Settings -> CI/CD -> Variables`;
- Найдите таблицу `CI/CD Variables` и нажмите `Add variable`;
- Добавьте переменные из следующей таблицы:

| Переменная | Значение |
| --- | ----------- |
| AWS_ACCESS_KEY_ID | `AWS Access Key ID`, который мы получили раннее, при создании учетной записи Администратора |
| AWS_SECRET_ACCESS_KEY | `AWS Secret Access Key`, который мы получили раннее, при создании учетной записи Администратора |
| AWS_ECR_URL | URL реестра образов `AWS ECR`. Получите его из переменной `ecr_url` вывода `Terraform` |
| AWS_ECR_REPO_NAME | Название репозитория в реестре образов `AWS ECR`. Получите его из переменной `ecr_repo_name` вывода `Terraform` |
| AWS_REGION | Название используемого региона. Получите его из переменной `region` вывода `Terraform` |

8. Далее выполните `commit` и `push` изменений сервиса `LoadGenerator` на `gitlab.com` , что вызовет удаление всех образов из `AWS ECR`, запуск [kaniko](https://github.com/GoogleContainerTools/kaniko) для сборки образа сервиса и загрузки его в `AWS ECR`, а также запуск `Helm chart` для развертывания сервиса `LoadGenerator` с актуальным образом.

   ```shell
   git add .
   git commit -m "init_test_commit"
   git push --set-upstream origin main
   ```

9. Результат выполнения можно увидеть по следующим доменам:

| Сервис | Субдомен |
| --- | ----------- |
| frontend | https://app.example.com |
| grafana | https://grafana.example.com |


